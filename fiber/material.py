__author__ = 'JOENY'


class Material(object):
    """
    Material Identify.
    """

    def __init__(self, **kwargs):
        self.keys = kwargs.get('keys')

    @property
    def id(self):
        return self.keys.get('id')

    @id.setter
    def id(self, value):
        """

        :param value:
        :return:
        """
        self.keys['id'] = value