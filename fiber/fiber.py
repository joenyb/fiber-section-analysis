__author__ = 'JOENY'


class Fiber(object):
    """
    Fiber object.
    """
    def __init__(self, **kwargs):
        self.keys = kwargs.get('keys')

    @property
    def id(self):
        return self.keys.get('id')

    @id.setter
    def id(self, value):
        self.keys['id'] = value

    @property
    def x(self):
        return self.keys.get('x')

    @x.setter
    def x(self, value):
        self.keys['x'] = value

    @property
    def y(self):
        return self.keys.get('y')

    @y.setter
    def y(self, value):
        self.keys['y'] = value

    @property
    def area(self):
        return self.keys.get('area')

    @area.setter
    def area(self, value):
        self.keys['area'] = value
